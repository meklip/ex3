#include <iostream>
#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/ml/ml.hpp>

#include "intToString.h"

using namespace std;
using namespace cv;

int main(int argc, char ** argv) {
    int iterations;
    string training_images_path;
    string output_path;

    if (argc != 4) {
        cout << "parameters missing" << endl;
        return 1;
    } else {
        training_images_path = argv[1];
        output_path = argv[2];
        iterations = atoi(argv[3]);
    }

    srand((uint)time(NULL));

    // prepare matrices for labels and hog descriptor output
    Mat training_labels(1, 1000 + 500 * 100, CV_32FC1);     // 1000 positive example images, 500 negative
    Mat training_data(1764, 1000 + 500 * 100, CV_32FC1);    // take 250 random coutouts of each negative image

    clock_t begin = clock();
    for (int i = 0; i < 1000; i++) {
        string image_name = intToString(i + 1, 5);
        string positive_image_path = training_images_path + "/positive/" + image_name + ".jpg";

        Mat positive_image = imread(positive_image_path, CV_LOAD_IMAGE_GRAYSCALE);
        resize(positive_image, positive_image, Size(64, 64));

        HOGDescriptor positive_desc; positive_desc.winSize = Size(64, 64); // set descriptor window size to 64x64
        vector<float> positive_desc_values;

        positive_desc.compute(positive_image, positive_desc_values);

        training_labels.at<float>(0, i) = 1.0f;

        Mat c = Mat_<float>(positive_desc_values, true);  // initialize a matrix with the hog descriptor vector
        c.col(0).copyTo(training_data.col(i));            // copy the row matrix to the training data matrix
    }

    int row_count = 1000;   // first 1000 entries are positive examples, start inserting into data matrix at 1000
    for (int i = 0; i < 500; i++) {
        string image_name = intToString(i + 1, 5);
        string negative_image_path = training_images_path + "/negative/" + image_name + ".jpg";

        Mat negative_image = imread(negative_image_path, CV_LOAD_IMAGE_GRAYSCALE);

        int width = negative_image.cols;
        int height = negative_image.rows;

        for (int j = 0; j < 100; j++) {
            // calculate a random cutout of the current image
            int x = rand() % (width - 10);      // cutout must be at least 10x10
            int y = rand() % (height - 10);     // so leave 10 px room

            int delta_x = width - x;
            int delta_y = height - y;

            int delta = (delta_x > delta_y) ? delta_y : delta_x;

            int edge_length = rand() % delta;

            if (edge_length > 200) edge_length = 200;
            if (edge_length < 10) edge_length = 10;

            Rect r(x, y, edge_length, edge_length);

            Mat cutout;
            negative_image(r).copyTo(cutout);

            resize(cutout, cutout, Size(64, 64));

            HOGDescriptor negative_desc;
            negative_desc.winSize = Size(64, 64);
            vector<float> negative_desc_values;

            negative_desc.compute(cutout, negative_desc_values);

            training_labels.at<float>(0, row_count) = -1.0f;

            Mat c = Mat_<float>(negative_desc_values, true);
            c.col(0).copyTo(training_data.col(row_count));

            row_count++;
        }
    }

    clock_t end = clock();
    cout << "Descriptor computation time: " << (end - begin) / (double)CLOCKS_PER_SEC << endl;

    transpose(training_labels, training_labels);
    transpose(training_data, training_data);

    // train and save the svm
    CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
    params.kernel_type = CvSVM::LINEAR;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, iterations, 1e-6);

    CvSVM svm;

    clock_t sbegin = clock();
    svm.train(training_data, training_labels, Mat(), Mat(), params);
    clock_t send = clock();

    cout << "SVM training time: " << (send - sbegin) / (double)CLOCKS_PER_SEC << endl;

    svm.save(output_path.c_str());

    return 0;
};
