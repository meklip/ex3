#include "detector.hpp"

#include <stdio.h>

using namespace std;
using namespace cv;

Detector::Detector(string svm_path) {
    svm.load(svm_path.c_str());
}

Detector::Detector(string image_path, string svm_path) {
    image = imread(image_path, CV_LOAD_IMAGE_GRAYSCALE);
    svm.load(svm_path.c_str());
}

vector<Rect> Detector::detect() {
    int width = image.cols;
    int height = image.rows;

    Rect best_rect;
    float best_response = 0;

    vector<Rect> rects;

    HOGDescriptor descriptor;
    descriptor.winSize = windowSize;

    Mat level;
    float scale = 1.0f * factor * factor * factor;

    while (width > windowSize.width && height > windowSize.height) {
        resize(image, level, Size(width, height));

        #pragma omp parallel for
        for (int i = 0; i < (height - windowSize.height - 20); i += 20) {
            for (int j = 0; j < (width - windowSize.width - 20); j += 20) {
                Mat c;
                level(Rect(j, i, windowSize.width, windowSize.height)).copyTo(c);

                vector<float> desc_values;
                descriptor.compute(c, desc_values, Size(0, 0), Size());

                Mat row = Mat_<float>(desc_values, true);
                transpose(row, row);

                float r_val = svm.predict(row, true);

                if (r_val < 0) {
                    int x = (int)round(j * scale);
                    int y = (int)round(i * scale);
                    int w = (int)round(windowSize.width * scale);
                    int h = (int)round(windowSize.height * scale);

                    #pragma omp critical
                    {
                        if (r_val < best_response) {
                            best_response = r_val;
                            best_rect = Rect(x, y, w, h);
                        }

                        rects.push_back(Rect(x, y, w, h));
                    }
                }
            }
        }

        scale *= (1 / factor);

        width *= factor;
        height *= factor;

        width = (int)round(width);
        height = (int)round(height);
    }

    rects.push_back(best_rect);

    return rects;
}