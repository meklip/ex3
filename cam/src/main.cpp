#include <iostream>
#include <ctime>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "detector.hpp"

using namespace std;
using namespace cv;

int main(int argc, char ** argv) {
    string svm_path;

    if (argc != 2) {
        cout << "arguments missing" << endl;
        return 1;
    } else {
        svm_path = argv[1];
    }

    VideoCapture cap(0);

    if (!cap.isOpened()) {
        return 1;
    }

    Detector detector(svm_path);
    detector.windowSize = Size(64, 64);
    detector.factor = 0.6f;

    namedWindow("Video", CV_WINDOW_AUTOSIZE);

    while (true) {
        Mat frame, gray;
        cap >> frame;

        cvtColor(frame, gray, CV_BGR2GRAY);

        detector.image = gray;
        vector<Rect> rects = detector.detect();

        rectangle(frame, rects.back(), Scalar(255, 0, 0), 2);

        imshow("Video", frame);

        if (waitKey(30) >= 0) break;
    }

    return 0;
}