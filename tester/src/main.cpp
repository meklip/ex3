#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/ml/ml.hpp>

#include "intToString.h"

using namespace std;
using namespace cv;

int main(int argc, char ** argv) {
    string test_images_path;
    string svm_path;

    if (argc != 3) {
        cout << "arguments missing" << endl;
        return 1;
    } else {
        test_images_path = argv[1];
        svm_path = argv[2];
    }

    Mat test_data(200, 1764, CV_32FC1);

    for (int i = 0; i < 100; i++) {
        string image_name = intToString(i + 1, 5);

        string positive_image_path = test_images_path + "/positive/" + image_name + ".jpg";

        Mat positive_image = imread(positive_image_path, CV_LOAD_IMAGE_GRAYSCALE);
        resize(positive_image, positive_image, Size(64, 64));

        HOGDescriptor positive_desc; positive_desc.winSize = Size(64, 64);
        vector<float> positive_desc_values;
        vector<Point> positive_locations;

        positive_desc.compute(positive_image, positive_desc_values,
                              Size(0, 0), Size(0, 0), positive_locations);

        Mat row = Mat_<float>(positive_desc_values, true);
        transpose(row, row);
        row.row(0).copyTo(test_data.row(i));

        string negative_image_path = test_images_path + "/negative/" + image_name + ".jpg";

        Mat negative_image = imread(negative_image_path, CV_LOAD_IMAGE_GRAYSCALE);
        resize(negative_image, negative_image, Size(64, 64));

        HOGDescriptor negative_desc;
        negative_desc.winSize = Size(64, 64);
        vector<float> negative_desc_values;
        vector<Point> negative_locations;

        negative_desc.compute(negative_image, negative_desc_values,
                              Size(0, 0), Size(0, 0), negative_locations);

        Mat n_row = Mat_<float>(negative_desc_values, true);
        transpose(n_row, n_row);
        n_row.row(0).copyTo(test_data.row(i + 100));
    }

    CvSVM svm;
    svm.load(svm_path.c_str());

    Mat results;
    svm.predict(test_data, results);

    cout << results << endl;

    int fp = 0;
    int fn = 0;

    for (int i = 0; i < 200; i++) {
        float result = results.at<float>(i, 0);

        if (i < 100) {
            if (result != 1) {
                ++fn;
            }
        } else {
            if (result != -1) {
                ++fp;
            }
        }
    }

    cout << "False positives: " << fp << endl;
    cout << "False negatives: " << fn << endl;

    return 0;
}