#include <string>
#include <iostream>
#include <stdio.h>  

//returns a string with specified number of leading zeros 
std::string intToString(int number, int minLength)
{
    std::stringstream ss; //create a stringstream
    ss << number; //add number to the stream
    std::string out = ss.str();
    while((int)out.length() < minLength) out = "0" + out;
    return out; //return a string with the contents of the stream
}
 
