cmake_minimum_required(VERSION 3.2)
project(tester)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(OpenCV REQUIRED core highgui imgproc objdetect ml)

set(SOURCE_FILES src/main.cpp)

add_executable(tester ${SOURCE_FILES} src/intToString.h)

target_link_libraries(tester ${OpenCV_LIBS})