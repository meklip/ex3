#include <iostream>

#include <opencv2/core/core.hpp>
#include <chrono>

#include "detector.hpp"

using namespace std;
using namespace cv;

int main(int argc, char ** argv) {
    string image_path;
    string svm_path;

    if (argc != 3) {
        cout << "paramters missing" << endl;
    } else {
        image_path = argv[1];
        svm_path = argv[2];
    }

    Detector detector(image_path, svm_path);

    detector.windowSize = Size(64, 64);
    detector.factor = 0.8f;

    auto begin = chrono::system_clock::now();
    vector<Rect> rects = detector.detect();
    auto end = chrono::system_clock::now();
    auto delta = chrono::duration_cast<chrono::milliseconds>(end - begin);

    cout << "Detection time: " << delta.count() << endl;

    Mat color_image = imread(image_path, CV_LOAD_IMAGE_COLOR);
    for (vector<Rect>::iterator it = rects.begin(); it != rects.end(); ++it) {
        rectangle(color_image, *it, Scalar(0,0,100), 1);
    }

    rectangle(color_image, rects.back(), Scalar(255, 0, 0), 2);

    //cv::imshow("Image", color_image);
    //waitKey(0);

    return 0;
}