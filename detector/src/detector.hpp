#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/ml/ml.hpp>


class Detector {
private:
    CvSVM svm;

public:
    cv::Mat image;

    cv::Size windowSize;
    float factor;

    Detector();
    Detector(std::string image_path, std::string svm_path);

    std::vector<cv::Rect> detect();
};